<?php

namespace UnicaenEnquete\Entity;

use UnicaenEnquete\Entity\Db\Instance;

interface HasEnqueteInterface
{

    //private ?Instance $enquete = null;

    public function getEnquete(): ?Instance;
    public function setEnquete(?Instance $enquete): void;

}