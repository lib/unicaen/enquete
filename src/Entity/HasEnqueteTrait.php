<?php

namespace UnicaenEnquete\Entity;

use DateTime;
use UnicaenEnquete\Entity\Db\Instance;

trait HasEnqueteTrait
{

    private ?Instance $enquete = null;

    public function getEnquete(): ?Instance
    {
        return $this->enquete;
    }

    public function setEnquete(?Instance $enquete): void
    {
        $this->enquete = $enquete;
    }

}