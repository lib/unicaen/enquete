<?php

namespace UnicaenEnquete\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Enquete implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    private ?int $id = null;
    private ?string $titre = null;
    private ?string $code = null;
    private ?string $description = null;
    private Collection $groupes;
    private Collection $questions;

    public function __construct()
    {
        $this->groupes = new ArrayCollection();
        $this->questions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitre(): ?string
    {
        return $this->titre;
    }

    public function setTitre(?string $titre): void
    {
        $this->titre = $titre;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getGroupes(): Collection
    {
        return $this->groupes;
    }

    public function getQuestions(): Collection
    {
        return $this->questions;
    }

    public function getQuestionsSansGroupes(): array
    {
        $questions = [];
        /** @var Question $question */
        foreach ($this->questions as $question) {
            if ($question->getGroupe() === null) $questions[] = $question;
        }
        return $questions;
    }

}