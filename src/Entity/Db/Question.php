<?php

namespace UnicaenEnquete\Entity\Db;

use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Question implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    private ?int $id = null;
    private ?string $libelle = null;
    private ?string $description = null;
    private ?Enquete $enquete = null;
    private ?Groupe $groupe = null;
    private ?int $ordre = null;
    private bool $hasNote = true;
    private bool $hasCommentaire = true;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getEnquete(): ?Enquete
    {
        return $this->enquete;
    }

    public function setEnquete(?Enquete $enquete): void
    {
        $this->enquete = $enquete;
    }

    public function getGroupe(): ?Groupe
    {
        return $this->groupe;
    }

    public function setGroupe(?Groupe $groupe): void
    {
        $this->groupe = $groupe;
    }

    public function getOrdre(): ?int
    {
        return $this->ordre;
    }

    public function setOrdre(?int $ordre): void
    {
        $this->ordre = $ordre;
    }

    public function hasNote(): bool
    {
        return $this->hasNote;
    }

    public function setHasNote(bool $hasNote): void
    {
        $this->hasNote = $hasNote;
    }

    public function hasCommentaire(): bool
    {
        return $this->hasCommentaire;
    }

    public function setHasCommentaire(bool $hasCommentaire): void
    {
        $this->hasCommentaire = $hasCommentaire;
    }

    /** MACROS ET HELPERS *********************************************************************************************/

    public function getSelectId(): string
    {
        return "question_" . $this->getId() . "_niveau";
    }

    public function getTextAreaId(): string
    {
        return "question_" . $this->getId() . "_commentaire";
    }
}