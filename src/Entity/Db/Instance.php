<?php

namespace UnicaenEnquete\Entity\Db;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareInterface;
use UnicaenUtilisateur\Entity\Db\HistoriqueAwareTrait;

class Instance implements HistoriqueAwareInterface
{
    use HistoriqueAwareTrait;

    private ?int $id = null;
    private ?Enquete $enquete = null;
    private Collection $reponses;
    private ?DateTime $validation = null;

    public function __construct()
    {
        $this->reponses = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEnquete(): ?Enquete
    {
        return $this->enquete;
    }

    public function setEnquete(?Enquete $enquete): void
    {
        $this->enquete = $enquete;
    }

    public function getReponses(): Collection
    {
        return $this->reponses;
    }

    public function getReponseFor(Question $question): ?Reponse
    {
        foreach ($this->reponses as $reponse) {
            if ($reponse->getQuestion() === $question and $reponse->estNonHistorise()) return $reponse;
        }
        return null;
    }

    public function getValidation(): ?DateTime
    {
        return $this->validation;
    }

    public function setValidation(?DateTime $validation): void
    {
        $this->validation = $validation;
    }

}