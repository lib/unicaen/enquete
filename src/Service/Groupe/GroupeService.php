<?php

namespace UnicaenEnquete\Service\Groupe;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use UnicaenEnquete\Entity\Db\Enquete;
use UnicaenEnquete\Entity\Db\Groupe;

class GroupeService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Groupe $groupe): Groupe
    {
        $this->getObjectManager()->persist($groupe);
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function update(Groupe $groupe): Groupe
    {
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function historise(Groupe $groupe): Groupe
    {
        $groupe->historiser();
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function restore(Groupe $groupe): Groupe
    {
        $groupe->dehistoriser();
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    public function delete(Groupe $groupe): Groupe
    {
        $this->getObjectManager()->remove($groupe);
        $this->getObjectManager()->flush($groupe);
        return $groupe;
    }

    /** REQUETAGE  ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Groupe::class)->createQueryBuilder('groupe')
            ->join('groupe.enquete', 'enquete')->addSelect('enquete')
            ->leftJoin('groupe.questions', 'question')->addSelect('question');
        return $qb;
    }

    public function getGroupe(?int $id): ?Groupe
    {
        $qb = $this->createQueryBuilder()->andWhere('groupe.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Groupe partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedGroupe(AbstractActionController $controller, string $param = 'groupe'): ?Groupe
    {
        $id = $controller->params()->fromRoute($param);
        $groupe = $this->getGroupe($id);
        return $groupe;
    }

    public function getGroupeByEnqueteAndCode(?Enquete $enquete, ?string $code): ?Groupe
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('groupe.code = :code')->setParameter('code', $code)
            ->andWhere('groupe.enquete = :enquete')->setParameter('enquete', $enquete);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Groupe partagent le même couple (enquete,code) [" . $enquete->getId() . "," . $code . "]", 0, $e);
        }
        return $result;
    }

    public function getGroupes(bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder();

        if (!$withHisto) {
            $qb = $qb->andWhere('groupe.histoDestruction IS NULL');
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getGroupesByEnquete(?Enquete $enquete, bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('groupe.enquete = :enquete')->setParameter('enquete', $enquete);

        if (!$withHisto) {
            $qb = $qb->andWhere('groupe.histoDestruction IS NULL');
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }


    public function getGroupesAsOptions(?Enquete $enquete): array
    {
        /** @var Groupe $groupes */
        $groupes = $this->getGroupesByEnquete($enquete);
        $options = [];
        foreach ($groupes as $groupe) {
            $options[$groupe->getId()] = $groupe->getLibelle();
        }
        return $options;
    }

    /** FACADE ********************************************************************************************************/

}