<?php

namespace UnicaenEnquete\Service\Enquete;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenEnquete\Entity\Db\Enquete;

class EnqueteService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITES  *************************************************************************/

    public function create(Enquete $enquete): Enquete
    {
        $this->getObjectManager()->persist($enquete);
        $this->getObjectManager()->flush();
        return $enquete;
    }

    public function update(Enquete $enquete): Enquete
    {
        $this->getObjectManager()->flush($enquete);
        return $enquete;
    }

    public function historise(Enquete $enquete): Enquete
    {
        $enquete->historiser();
        $this->getObjectManager()->flush($enquete);
        return $enquete;
    }

    public function restore(Enquete $enquete): Enquete
    {
        $enquete->dehistoriser();
        $this->getObjectManager()->flush($enquete);
        return $enquete;
    }

    public function delete(Enquete $enquete): Enquete
    {
        $this->getObjectManager()->remove($enquete);
        $this->getObjectManager()->flush($enquete);
        return $enquete;
    }

    /** QUERYING ******************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Enquete::class)->createQueryBuilder('enquete');
        return $qb;
    }

    public function getEnquete(?int $id = null): ?Enquete
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('enquete.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Enquete::class . "] partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedEnquete(AbstractActionController $controller, string $param = 'enquete'): ?Enquete
    {
        $id = $controller->params()->fromRoute($param);
        $result = $this->getEnquete($id);
        return $result;
    }

    public function getEnqueteByCode(?string $code): ?Enquete
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('enquete.code = :code')->setParameter('code', $code);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Enquete::class . "] partagent le même code [" . $code . "]", 0, $e);
        }
        return $result;
    }


    public function getEnquetes(string $champ = 'titre', string $order = 'ASC', bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('enquete.' . $champ, $order);

        if (!$withHisto) $qb = $qb->andWhere('enquete.histoDestruction IS NULL');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getEnquetesAsOptions(bool $withHisto = false): array
    {
        $enquetes = $this->getEnquetes('titre', 'ASC', $withHisto);
        $options = [];
        foreach ($enquetes as $enquete) {
            $options[$enquete->getId()] = $enquete->getTitre();
        }
        return $options;
    }

    /** FACADE ********************************************************************************************************/
}