<?php

namespace UnicaenEnquete\Service\Enquete;

trait EnqueteServiceAwareTrait
{

    private EnqueteService $enqueteService;

    public function getEnqueteService(): EnqueteService
    {
        return $this->enqueteService;
    }

    public function setEnqueteService(EnqueteService $enqueteService): void
    {
        $this->enqueteService = $enqueteService;
    }

}