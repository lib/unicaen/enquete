<?php

namespace UnicaenEnquete\Service\Resultat;

trait ResultatServiceAwareTrait
{

    private ResultatService $resultatService;

    public function getResultatService(): ResultatService
    {
        return $this->resultatService;
    }

    public function setResultatService(ResultatService $resultatService): void
    {
        $this->resultatService = $resultatService;
    }

}