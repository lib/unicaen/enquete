<?php

namespace UnicaenEnquete\Service\Resultat;

use UnicaenEnquete\Entity\Db\Enquete;
use UnicaenEnquete\Entity\Db\Instance;
use UnicaenEnquete\Entity\Db\Question;
use UnicaenEnquete\Entity\HasEnqueteInterface;

class ResultatService
{

    /**
     * @param Enquete $enquete
     * @param HasEnqueteInterface[] $resultats
     * @return array
     */
    public function generateResultatArray(Enquete $enquete, array $resultats): array
    {

        $counts = [];
        $results = [];
        $comments = [];

        /** @var Question $question */
        foreach ($enquete->getQuestions() as $question) {
            $counts[$question->getId()]["sans"] = [];
            $counts[$question->getId()]["avec"] = [];
            $results[$question->getId()] = [];

            if ($question->estNonHistorise() and ($question->getGroupe() === null or $question->getGroupe()->estNonHistorise())) {
                /** @var Instance $item */
                foreach ($resultats as $item) {
                    if ($item->estNonHistorise()) {
                        $reponse = null;
                        if ($item instanceof Instance) $reponse =  $item->getReponseFor($question);
                        if ($item instanceof HasEnqueteInterface AND $item->getEnquete() AND $item->getEnquete()->getValidation()) $reponse = $item->getEnquete()->getReponseFor($question);
                        if ($reponse) {
                            if ($reponse->getReponse()) $results[$question->getId()][$reponse->getReponse()][] = $item->getId();
                            if ($reponse->getCommentaire()) $comments[$question->getId()][] = [$reponse->getReponse(), html_entity_decode(strip_tags($reponse->getCommentaire()))];
                            if ($reponse->getReponse() === "0") {
                                $counts[$question->getId()]["sans"][] = $item->getId();
                            } else {
                                $counts[$question->getId()]["avec"][] = $item->getId();
                            }
                        }
                    }
                }
            }
        }
        return [$counts, $results, $comments];
    }
}