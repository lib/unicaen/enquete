<?php

namespace UnicaenEnquete\Service\Resultat;

use Psr\Container\ContainerInterface;

class ResultatServiceFactory
{

    public function __invoke(ContainerInterface $container): ResultatService
    {
        $service = new ResultatService();
        return $service;
    }
}