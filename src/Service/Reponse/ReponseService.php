<?php

namespace UnicaenEnquete\Service\Reponse;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use UnicaenEnquete\Entity\Db\Instance;
use UnicaenEnquete\Entity\Db\Question;
use UnicaenEnquete\Entity\Db\Reponse;

class ReponseService
{
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Reponse $reponse): Reponse
    {
        $this->getObjectManager()->persist($reponse);
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    public function update(Reponse $reponse): Reponse
    {
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    public function historise(Reponse $reponse): Reponse
    {
        $reponse->historiser();
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    public function restore(Reponse $reponse): Reponse
    {
        $reponse->dehistoriser();
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    public function delete(Reponse $reponse): Reponse
    {
        $this->getObjectManager()->remove($reponse);
        $this->getObjectManager()->flush($reponse);
        return $reponse;
    }

    /** REQUETAGE  ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Reponse::class)->createQueryBuilder('reponse')
            ->leftJoin('reponse.question', 'question')->addSelect('question')
            ->leftJoin('question.enquete', 'enquete')->addSelect('enquete')
            ->leftJoin('question.groupe', 'groupe')->addSelect('groupe');
        return $qb;
    }

    /**
     * @param int|null $id
     * @return Reponse|null
     */
    public function getReponse(?int $id): ?Reponse
    {
        $qb = $this->createQueryBuilder()->andWhere('reponse.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Reponse partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return Reponse|null
     */
    public function getRequestedReponse(AbstractActionController $controller, string $param = 'reponse'): ?Reponse
    {
        $id = $controller->params()->fromRoute($param);
        $categorie = $this->getReponse($id);
        return $categorie;
    }

    /** @return Reponse[] */
    public function getReponses(bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder();
        if ($withHisto) $qb = $qb->andWhere('reponse.histoDestruction IS NULL');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Reponse[] */
    public function getReponsesByInstance(?Instance $instance, bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('reponse.instance = :instance')->setParameter('instance', $instance);
        if ($withHisto) $qb = $qb->andWhere('reponse.histoDestruction IS NULL');
        $result = $qb->getQuery()->getResult();

        $dictionnaire = [];
        foreach ($result as $item) {
            $dictionnaire[$item->getQuestion()->getId()] = $item;
        }
        return $dictionnaire;
    }

    /** FACADE ********************************************************************************************************/

    public function createReponse(Instance $instance, Question $question, ?string $tReponse, ?string $commentaire = null): Reponse
    {
        $reponse = new Reponse();
        $reponse->setInstance($instance);
        $reponse->setQuestion($question);
        $reponse->setReponse($tReponse);
        $reponse->setCommentaire($commentaire);
        $this->create($reponse);
        return $reponse;
    }
}