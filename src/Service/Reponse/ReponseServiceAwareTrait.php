<?php

namespace UnicaenEnquete\Service\Reponse;

trait ReponseServiceAwareTrait
{
    private ReponseService $reponseService;

    public function getReponseService(): ReponseService
    {
        return $this->reponseService;
    }

    public function setReponseService(ReponseService $reponseService): void
    {
        $this->reponseService = $reponseService;
    }

}