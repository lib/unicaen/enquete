<?php

namespace UnicaenEnquete\Service\Question;

trait QuestionServiceAwareTrait
{
    private QuestionService $questionService;

    public function getQuestionService(): QuestionService
    {
        return $this->questionService;
    }

    public function setQuestionService(QuestionService $questionService): void
    {
        $this->questionService = $questionService;
    }

}