<?php

namespace UnicaenEnquete\Service\Question;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use UnicaenEnquete\Entity\Db\Question;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;

class QuestionService {
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Question $question) : Question
    {
        $this->getObjectManager()->persist($question);
        $this->getObjectManager()->flush($question);
        return $question;
    }

    public function update(Question $question) : Question
    {
        $this->getObjectManager()->flush($question);
        return $question;
    }

    public function historise(Question $question) : Question
    {
        $question->historiser();
        $this->getObjectManager()->flush($question);
        return $question;
    }

    public function restore(Question $question) : Question
    {
        $question->dehistoriser();
        $this->getObjectManager()->flush($question);
        return $question;
    }

    public function delete(Question $question) : Question
    {
        $this->getObjectManager()->remove($question);
        $this->getObjectManager()->flush($question);
        return $question;
    }

    /** REQUETAGE  ****************************************************************************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Question::class)->createQueryBuilder('question')
            ->leftJoin('question.enquete', 'enquete')->addSelect('enquete')
            ->leftJoin('question.groupe', 'groupe')->addSelect('groupe')
        ;
        return $qb;
    }

    /**
     * @param int|null $id
     * @return Question|null
     */
    public function getQuestion(?int $id) : ?Question
    {
        $qb = $this->createQueryBuilder()->andWhere('question.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Question partagent le même id [".$id."]", 0 , $e);
        }
        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $param
     * @return Question|null
     */
    public function getRequestedQuestion(AbstractActionController $controller, string $param = 'question') : ?Question
    {
        $id = $controller->params()->fromRoute($param);
        $categorie = $this->getQuestion($id);
        return $categorie;
    }

    public function getQuestions(bool $withHisto = false)
    {
        $qb = $this->createQueryBuilder();
        if ($withHisto) $qb = $qb->andWhere('question.histoDestruction IS NULL')
    ;
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FACADE ********************************************************************************************************/

}