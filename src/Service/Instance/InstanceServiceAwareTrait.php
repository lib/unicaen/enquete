<?php

namespace UnicaenEnquete\Service\Instance;

trait InstanceServiceAwareTrait
{
    private InstanceService $instanceService;

    public function getInstanceService(): InstanceService
    {
        return $this->instanceService;
    }

    public function setInstanceService(InstanceService $instanceService): void
    {
        $this->instanceService = $instanceService;
    }

}