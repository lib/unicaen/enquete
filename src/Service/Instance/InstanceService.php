<?php

namespace UnicaenEnquete\Service\Instance;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Exception\RuntimeException;
use UnicaenEnquete\Entity\Db\Enquete;
use UnicaenEnquete\Entity\Db\Instance;
use UnicaenEnquete\Entity\Db\Question;
use UnicaenEnquete\Entity\Db\Reponse;
use UnicaenEnquete\Service\Reponse\ReponseServiceAwareTrait;

class InstanceService
{
    use ProvidesObjectManager;
    use ReponseServiceAwareTrait;

    /** GESTION DES ENTITES *******************************************************************************************/

    public function create(Instance $instance): Instance
    {
        $this->getObjectManager()->persist($instance);
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    public function update(Instance $instance): Instance
    {
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    public function historise(Instance $instance): Instance
    {
        $instance->historiser();
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    public function restore(Instance $instance): Instance
    {
        $instance->dehistoriser();
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    public function delete(Instance $instance): Instance
    {
        $this->getObjectManager()->remove($instance);
        $this->getObjectManager()->flush($instance);
        return $instance;
    }

    /** REQUETAGE  ****************************************************************************************************/

    public function createQueryBuilder(): QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Instance::class)->createQueryBuilder('einstance')
            ->join('einstance.enquete', 'enquete')->addSelect('enquete')
            ->leftJoin('einstance.reponses', 'reponse')->addSelect('reponse');
        return $qb;
    }

    public function getInstance(?int $id): ?Instance
    {
        $qb = $this->createQueryBuilder()->andWhere('einstance.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs [" . Instance::class . "] partagent le même id [" . $id . "]", 0, $e);
        }
        return $result;
    }

    public function getRequestedInstance(AbstractActionController $controller, string $param = 'instance'): ?Instance
    {
        $id = $controller->params()->fromRoute($param);
        $instance = $this->getInstance($id);
        return $instance;
    }

    public function getInstances(bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder();

        if (!$withHisto) {
            $qb = $qb->andWhere('einstance.histoDestruction IS NULL');
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getInstancesByEnquete(?Enquete $enquete, bool $withHisto = false): array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('einstance.enquete = :enquete')->setParameter('enquete', $enquete);

        if (!$withHisto) {
            $qb = $qb->andWhere('einstance.histoDestruction IS NULL');
        }
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** FACADE ********************************************************************************************************/

    public function createInstance(Enquete $enquete): Instance
    {
        $instance = new Instance();
        $instance->setEnquete($enquete);
        $this->create($instance);
        return $instance;
    }

    public function updateReponses(Instance $instance, $data): void
    {
        $reponses = $instance->getReponses()->toArray();
        $reponses = array_filter($reponses, function (Reponse $reponse) {
            return $reponse->estNonHistorise();
        });

        /** @var Reponse[] $dictionnaireReponses */
        $dictionnaireReponses = [];
        foreach ($reponses as $reponse) {
            $dictionnaireReponses[$reponse->getQuestion()->getId()] = $reponse;
        }

        /** @var Question $question */
        foreach ($instance->getEnquete()->getQuestions() as $question) {
            if ($question->estNonHistorise() and ($question->getGroupe() === null or $question->getGroupe()->estNonHistorise())) {
                $select = $data[$question->getSelectId()] ?? null;
                $textarea = $data[$question->getTextAreaId()] ?? null;
                //nouvelle
                if (($select !== null or $textarea !== null) and !isset($dictionnaireReponses[$question->getId()])) {
                    $this->getReponseService()->createReponse($instance, $question, $select, $textarea);
                }
                //modification
                if (($select !== null or $textarea !== null) and isset($dictionnaireReponses[$question->getId()])) {
                    $dictionnaireReponses[$question->getId()]->setReponse($select);
                    $dictionnaireReponses[$question->getId()]->setCommentaire($textarea);
                    $this->getReponseService()->update($dictionnaireReponses[$question->getId()]);
                }
                //suppresion
                if ($select === null and $textarea === null and isset($dictionnaireReponses[$question->getId()])) {
                    $this->getReponseService()->historise($dictionnaireReponses[$question->getId()]);
                }
            }
        }
    }

}