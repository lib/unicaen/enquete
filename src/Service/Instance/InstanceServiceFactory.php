<?php

namespace UnicaenEnquete\Service\Instance;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Reponse\ReponseService;

class InstanceServiceFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InstanceService
    {
        /**
         * @var EntityManager $entityManager
         * @var ReponseService $reponseService
         */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');
        $reponseService = $container->get(ReponseService::class);

        $service = new InstanceService();
        $service->setObjectManager($entityManager);
        $service->setReponseService($reponseService);
        return $service;
    }
}