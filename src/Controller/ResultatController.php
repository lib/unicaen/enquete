<?php

namespace UnicaenEnquete\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Instance\InstanceServiceAwareTrait;
use UnicaenEnquete\Service\Resultat\ResultatServiceAwareTrait;

class ResultatController extends AbstractActionController
{
    use EnqueteServiceAwareTrait;
    use InstanceServiceAwareTrait;
    use ResultatServiceAwareTrait;


    public function resultatsAction(): ViewModel
    {
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);

        $instances = $this->getInstanceService()->getInstances();

        $resultats = [];
        foreach ($instances as $instance) {
            if ($instance !== null && $instance->getValidation() !== null) $resultats[] = $instance;
        }

        [$counts, $results] = $this->getResultatService()->generateResultatArray($enquete, $resultats);

        return new ViewModel([
            'enquete' => $enquete,
            'results' => $results,
            'counts' => $counts,
            'elements' => $instances,
        ]);
    }
}