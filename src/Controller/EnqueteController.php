<?php

namespace UnicaenEnquete\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEnquete\Entity\Db\Enquete;
use UnicaenEnquete\Form\Enquete\EnqueteFormAwareTrait;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;

class EnqueteController extends AbstractActionController
{
    use EnqueteServiceAwareTrait;
    use EnqueteFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $enquetes = $this->getEnqueteService()->getEnquetes('titre', 'ASC', true);

        return new ViewModel([
            'enquetes' => $enquetes
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);

        return new ViewModel([
            'enquete' => $enquete
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $enquete = new Enquete();

        $form = $this->getEnqueteForm();
        $form->setAttribute('action', $this->url()->fromRoute('enquete/enquete/ajouter', [], [], true));
        $form->bind($enquete);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEnqueteService()->create($enquete);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajout d'une enquete",
            'form' => $form
        ]);
        $vm->setTemplate('unicaen-enquete/default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);

        $form = $this->getEnqueteForm();
        $form->setAttribute('action', $this->url()->fromRoute('enquete/enquete/modifier', ['enquete' => $enquete->getId()], [], true));
        $form->bind($enquete);
        $form->setOldCode($enquete->getCode());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getEnqueteService()->update($enquete);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modification d'une enquete",
            'form' => $form
        ]);
        $vm->setTemplate('unicaen-enquete/default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);
        $this->getEnqueteService()->historise($enquete);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/enquete');
    }

    public function restaurerAction(): Response
    {
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);
        $this->getEnqueteService()->restore($enquete);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/enquete');
    }

    public function supprimerAction()
    {
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getEnqueteService()->delete($enquete);
            exit();
        }

        $vm = new ViewModel();
        if ($enquete !== null) {
            $vm->setTemplate('unicaen-enquete/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de l'enquête [" . $enquete->getTitre() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('enquete/enquete/supprimer', ["enquete" => $enquete->getId()], [], true),
            ]);
        }
        return $vm;
    }
}