<?php

namespace UnicaenEnquete\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEnquete\Entity\Db\Groupe;
use UnicaenEnquete\Form\Groupe\GroupeFormAwareTrait;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Groupe\GroupeServiceAwareTrait;

class GroupeController extends AbstractActionController
{
    use EnqueteServiceAwareTrait;
    use GroupeServiceAwareTrait;
    use GroupeFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $groupes = $this->getGroupeService()->getGroupes(true);

        return new ViewModel([
            'groupes' => $groupes
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);

        return new ViewModel([
            'groupe' => $groupe
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $groupe = new Groupe();
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);
        if ($enquete) $groupe->setEnquete($enquete);

        $form = $this->getGroupeForm();
        $form->setAttribute('action', $this->url()->fromRoute('enquete/groupe/ajouter', ['enquete' => $enquete?->getId() ], [], true));
        $form->bind($groupe);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getGroupeService()->create($groupe);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajout d'un groupe",
            'form' => $form
        ]);
        $vm->setTemplate('unicaen-enquete/default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);

        $form = $this->getGroupeForm();
        $form->setAttribute('action', $this->url()->fromRoute('enquete/groupe/modifier', ['groupe' => $groupe->getId()], [], true));
        $form->bind($groupe);
        $form->setOldCode($groupe->getCode());

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getGroupeService()->update($groupe);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modification d'un groupe",
            'form' => $form
        ]);
        $vm->setTemplate('unicaen-enquete/default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);
        $this->getGroupeService()->historise($groupe);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/groupe');
    }

    public function restaurerAction(): Response
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);
        $this->getGroupeService()->restore($groupe);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/groupe');
    }

    public function supprimerAction()
    {
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getGroupeService()->delete($groupe);
            exit();
        }

        $vm = new ViewModel();
        if ($groupe !== null) {
            $vm->setTemplate('unicaen-enquete/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du groupe [" . $groupe->getLibelle() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('enquete/groupe/supprimer', ["groupe" => $groupe->getId()], [], true),
            ]);
        }
        return $vm;
    }
}