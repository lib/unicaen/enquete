<?php

namespace UnicaenEnquete\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Form\Enquete\EnqueteForm;
use UnicaenEnquete\Service\Enquete\EnqueteService;

class EnqueteControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EnqueteController
    {
        /**
         * @var EnqueteService $enqueteService
         * @var EnqueteForm $enqueteForm
         */
        $enqueteService = $container->get(EnqueteService::class);
        $enqueteForm = $container->get('FormElementManager')->get(EnqueteForm::class);

        $controller = new EnqueteController();
        $controller->setEnqueteService($enqueteService);
        $controller->setEnqueteForm($enqueteForm);
        return $controller;
    }
}