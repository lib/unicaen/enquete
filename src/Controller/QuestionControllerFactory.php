<?php

namespace UnicaenEnquete\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Form\Question\QuestionForm;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Groupe\GroupeService;
use UnicaenEnquete\Service\Question\QuestionService;

class QuestionControllerFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): QuestionController
    {
        /**
         * @var EnqueteService $enqueteService
         * @var GroupeService $groupeService
         * @var QuestionService $questionService
         * @var QuestionForm $groupeForm
         */
        $enqueteService = $container->get(EnqueteService::class);
        $groupeService = $container->get(GroupeService::class);
        $questionService = $container->get(QuestionService::class);
        $questionForm = $container->get('FormElementManager')->get(QuestionForm::class);

        $controller = new QuestionController();
        $controller->setEnqueteService($enqueteService);
        $controller->setGroupeService($groupeService);
        $controller->setQuestionService($questionService);
        $controller->setQuestionForm($questionForm);
        return $controller;
    }
}