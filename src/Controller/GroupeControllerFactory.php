<?php

namespace UnicaenEnquete\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Form\Groupe\GroupeForm;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Groupe\GroupeService;

class GroupeControllerFactory
{
    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): GroupeController
    {
        /**
         * @var EnqueteService $enqueteService
         * @var GroupeService $groupeService
         * @var GroupeForm $groupeForm
         */
        $enqueteService = $container->get(EnqueteService::class);
        $groupeService = $container->get(GroupeService::class);
        $groupeForm = $container->get('FormElementManager')->get(GroupeForm::class);

        $controller = new GroupeController();
        $controller->setEnqueteService($enqueteService);
        $controller->setGroupeForm($groupeForm);
        $controller->setGroupeService($groupeService);
        return $controller;
    }
}