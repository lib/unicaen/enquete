<?php

namespace UnicaenEnquete\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Form\Instance\InstanceForm;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Instance\InstanceService;
use UnicaenEnquete\Service\Reponse\ReponseService;

class InstanceControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InstanceController
    {
        /**
         * @var EnqueteService $enqueteService
         * @var InstanceService $instanceService
         * @var ReponseService $reponseService
         * @var InstanceForm $instanceForm
         */
        $enqueteService = $container->get(EnqueteService::class);
        $instanceService = $container->get(InstanceService::class);
        $reponseService = $container->get(ReponseService::class);
        $instanceForm = $container->get('FormElementManager')->get(InstanceForm::class);

        $controller = new InstanceController();
        $controller->setEnqueteService($enqueteService);
        $controller->setInstanceService($instanceService);
        $controller->setReponseService($reponseService);
        $controller->setInstanceForm($instanceForm);
        return $controller;
    }

}