<?php

namespace UnicaenEnquete\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Instance\InstanceService;
use UnicaenEnquete\Service\Resultat\ResultatService;

class ResultatControllerFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): ResultatController
    {
        /**
         * @var EnqueteService $enqueteService
         * @var InstanceService $instanceService
         * @var ResultatService $resultatService
         */
        $enqueteService = $container->get(EnqueteService::class);
        $instanceService = $container->get(InstanceService::class);
        $resultatService = $container->get(ResultatService::class);

        $controller = new ResultatController();
        $controller->setEnqueteService($enqueteService);
        $controller->setInstanceService($instanceService);
        $controller->setResultatService($resultatService);

        return $controller;
    }
}