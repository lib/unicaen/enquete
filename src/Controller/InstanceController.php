<?php

namespace UnicaenEnquete\Controller;

use DateTime;
use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEnquete\Entity\Db\Instance;
use UnicaenEnquete\Form\Instance\InstanceFormAwareTrait;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Instance\InstanceServiceAwareTrait;
use UnicaenEnquete\Service\Reponse\ReponseServiceAwareTrait;

class InstanceController extends AbstractActionController
{
    use EnqueteServiceAwareTrait;
    use InstanceServiceAwareTrait;
    use ReponseServiceAwareTrait;
    use InstanceFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $instances = $this->getInstanceService()->getInstances(true);

        return new ViewModel([
            'instances' => $instances,
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $instance = $this->getInstanceService()->getRequestedInstance($this);

        return new ViewModel([
            'instance' => $instance,
        ]);
    }

    public function ajouterAction(): Response|ViewModel
    {
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);
        if ($enquete) {
            $instance = $this->getInstanceService()->createInstance($enquete);
            return $this->redirect()->toRoute('enquete/instance/modifier', ['instance' => $instance->getId()], [], true);
        }

        $instance = new Instance();
        $form = $this->getInstanceForm();
        $form->setAttribute('action', $this->url()->fromRoute('enquete/instance/ajouter', [], [], true));
        $form->bind($instance);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getInstanceService()->create($instance);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajout d'une instance",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-enquete/default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel|Response
    {
        $instance = $this->getInstanceService()->getRequestedInstance($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $this->getInstanceService()->updateReponses($instance, $data);

            if (isset($data["valider"])) {
                $instance->setValidation(new DateTime());
                $this->getInstanceService()->update($instance);
            }
        }

        $retour = $this->params()->fromQuery('retour');
        if ($retour) {
            return $this->redirect()->toUrl($retour);
        }
        return new ViewModel([
            'instance' => $instance,
        ]);
    }

    public function historiserAction(): Response
    {
        $instance = $this->getInstanceService()->getRequestedInstance($this);
        $this->getInstanceService()->historise($instance);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/instance', [], [], true);
    }

    public function restaurer(): Response
    {
        $instance = $this->getInstanceService()->getRequestedInstance($this);
        $this->getInstanceService()->restore($instance);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/instance', [], [], true);
    }

    public function supprimerAction(): ViewModel
    {
        $instance = $this->getInstanceService()->getRequestedInstance($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getInstanceService()->delete($instance);
            exit();
        }

        $vm = new ViewModel();
        if ($instance !== null) {
            $vm->setTemplate('unicaen-enquete/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression de l'instance  [" . $instance->getId() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('enquete/instance/supprimer', ["instance" => $instance->getId()], [], true),
            ]);
        }
        return $vm;
    }
}