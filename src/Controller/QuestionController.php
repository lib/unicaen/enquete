<?php

namespace UnicaenEnquete\Controller;

use Laminas\Http\Response;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEnquete\Entity\Db\Question;
use UnicaenEnquete\Form\Question\QuestionFormAwareTrait;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Groupe\GroupeServiceAwareTrait;
use UnicaenEnquete\Service\Question\QuestionServiceAwareTrait;

class QuestionController extends AbstractActionController
{
    use EnqueteServiceAwareTrait;
    use GroupeServiceAwareTrait;
    use QuestionServiceAwareTrait;
    use QuestionFormAwareTrait;

    public function indexAction(): ViewModel
    {
        $questions = $this->getQuestionService()->getQuestions(true);

        return new ViewModel([
            'questions' => $questions
        ]);
    }

    public function afficherAction(): ViewModel
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);

        return new ViewModel([
            'question' => $question
        ]);
    }

    public function ajouterAction(): ViewModel
    {
        $question = new Question();
        $enquete = $this->getEnqueteService()->getRequestedEnquete($this);
        $groupe = $this->getGroupeService()->getRequestedGroupe($this);
        if ($enquete) $question->setEnquete($enquete);
        if ($groupe) $question->setGroupe($groupe);

        $form = $this->getQuestionForm();
        $form->setAttribute('action', $this->url()->fromRoute('enquete/question/ajouter', ['enquete' => $enquete?->getId(), 'groupe' => $groupe?->getId()], [], true));
        $form->bind($question);
        if ($enquete) $form->reinitGroupesWithEnquete($enquete);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getQuestionService()->create($question);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Ajout d'une question",
            'form' => $form
        ]);
        $vm->setTemplate('unicaen-enquete/default/default-form');
        return $vm;
    }

    public function modifierAction(): ViewModel
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);
        $enquete = $question->getEnquete();

        $form = $this->getQuestionForm();
        $form->setAttribute('action', $this->url()->fromRoute('enquete/question/modifier', ['question' => $question->getId()], [], true));
        $form->bind($question);
        if ($enquete) $form->reinitGroupesWithEnquete($enquete);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getQuestionService()->update($question);
                exit();
            }
        }

        $vm = new ViewModel([
            'title' => "Modification d'un groupe",
            'form' => $form
        ]);
        $vm->setTemplate('unicaen-enquete/default/default-form');
        return $vm;
    }

    public function historiserAction(): Response
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);
        $this->getQuestionService()->historise($question);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/question');
    }

    public function restaurerAction(): Response
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);
        $this->getQuestionService()->restore($question);

        $retour = $this->params()->fromQuery('retour');
        if ($retour) return $this->redirect()->toUrl($retour);
        return $this->redirect()->toRoute('enquete/question');
    }

    public function supprimerAction(): ViewModel
    {
        $question = $this->getQuestionService()->getRequestedQuestion($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getQuestionService()->delete($question);
            exit();
        }

        $vm = new ViewModel();
        if ($question !== null) {
            $vm->setTemplate('unicaen-enquete/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression d'une question  [" . $question->getLibelle() . "]",
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('enquete/question/supprimer', ["question" => $question->getId()], [], true),
            ]);
        }
        return $vm;
    }
}