<?php

namespace UnicaenEnquete\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class QuestionPrivileges extends Privileges
{
    const QUESTION_INDEX = 'question-question_index';
    const QUESTION_AFFICHER = 'question-question_afficher';
    const QUESTION_AJOUTER = 'question-question_ajouter';
    const QUESTION_MODIFIER = 'question-question_modifier';
    const QUESTION_HISTORISER = 'question-question_historiser';
    const QUESTION_SUPPRIMER = 'question-question_supprimer';
}