<?php

namespace UnicaenEnquete\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EgroupePrivileges extends Privileges
{
    const GROUPE_INDEX = 'egroupe-groupe_index';
    const GROUPE_AFFICHER = 'egroupe-groupe_afficher';
    const GROUPE_AJOUTER = 'egroupe-groupe_ajouter';
    const GROUPE_MODIFIER = 'egroupe-groupe_modifier';
    const GROUPE_HISTORISER = 'egroupe-groupe_historiser';
    const GROUPE_SUPPRIMER = 'egroupe-groupe_supprimer';
}