<?php

namespace UnicaenEnquete\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EinstancePrivileges extends Privileges
{
    const INSTANCE_INDEX = 'einstance-instance_index';
    const INSTANCE_AFFICHER = 'einstance-instance_afficher';
    const INSTANCE_AJOUTER = 'einstance-instance_ajouter';
    const INSTANCE_MODIFIER = 'einstance-instance_modifier';
    const INSTANCE_HISTORISER = 'einstance-instance_historiser';
    const INSTANCE_SUPPRIMER = 'einstance-instance_supprimer';
}
