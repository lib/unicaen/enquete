<?php

namespace UnicaenEnquete\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class EnquetePrivileges extends Privileges
{
    const ENQUETE_INDEX = 'enquete-enquete_index';
    const ENQUETE_AFFICHER = 'enquete-enquete_afficher';
    const ENQUETE_AJOUTER = 'enquete-enquete_ajouter';
    const ENQUETE_MODIFIER = 'enquete-enquete_modifier';
    const ENQUETE_HISTORISER = 'enquete-enquete_historiser';
    const ENQUETE_SUPPRIMER = 'enquete-enquete_supprimer';
}
