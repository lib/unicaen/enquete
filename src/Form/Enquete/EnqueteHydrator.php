<?php

namespace UnicaenEnquete\Form\Enquete;

use Laminas\Hydrator\HydratorInterface;
use UnicaenEnquete\Entity\Db\Enquete;

class EnqueteHydrator implements HydratorInterface
{

    public function extract(object $object): array
    {
        /** @var Enquete $object */
        $data = [
            'titre' => $object->getTitre(),
            'code' => $object->getCode(),
            'description' => $object->getDescription(),
        ];
        return $data;
    }

    public function hydrate(array $data, object $object): object
    {
        $titre = (isset($data['titre']) and trim($data['titre']) !== null) ? trim($data['titre']) : null;
        $code = (isset($data['code']) and trim($data['code']) !== null) ? trim($data['code']) : null;
        $description = (isset($data['description']) and trim($data['description']) !== null) ? trim($data['description']) : null;

        /** @var Enquete $object */
        $object->setTitre($titre);
        $object->setCode($code);
        $object->setDescription($description);
        return $object;
    }


}