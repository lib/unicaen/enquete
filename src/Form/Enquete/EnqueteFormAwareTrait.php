<?php

namespace UnicaenEnquete\Form\Enquete;

trait EnqueteFormAwareTrait
{

    private EnqueteForm $enqueteForm;

    public function getEnqueteForm(): EnqueteForm
    {
        return $this->enqueteForm;
    }

    public function setEnqueteForm(EnqueteForm $enqueteForm): void
    {
        $this->enqueteForm = $enqueteForm;
    }

}