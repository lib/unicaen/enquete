<?php

namespace UnicaenEnquete\Form\Enquete;

use UnicaenEnquete\Service\Enquete\EnqueteService;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class EnqueteFormFactory {

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): EnqueteForm
    {
        /**
         * @var EnqueteService $enqueteService
         * @var EnqueteHydrator $hydrator
         */
        $enqueteService = $container->get(EnqueteService::class);
        $hydrator = $container->get('HydratorManager')->get(EnqueteHydrator::class);

        $form = new EnqueteForm();
        $form->setEnqueteService($enqueteService);
        $form->setHydrator($hydrator);
        return $form;
    }
}