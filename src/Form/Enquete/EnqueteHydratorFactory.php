<?php

namespace UnicaenEnquete\Form\Enquete;

use Psr\Container\ContainerInterface;

class EnqueteHydratorFactory
{
    public function __invoke(ContainerInterface $container): EnqueteHydrator
    {
        $hydrator = new EnqueteHydrator();
        return $hydrator;
    }
}