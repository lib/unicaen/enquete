<?php

namespace UnicaenEnquete\Form\Question;

use Laminas\Hydrator\HydratorInterface;
use UnicaenEnquete\Entity\Db\Question;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Groupe\GroupeServiceAwareTrait;

class QuestionHydrator implements HydratorInterface
{
    use EnqueteServiceAwareTrait;
    use GroupeServiceAwareTrait;

    public function extract(object $object): array
    {
        /** @var Question $object */
        $data = [
            'libelle' => $object->getLibelle(),
            'description' => $object->getDescription(),
            'enquete' => $object->getEnquete()?->getId(),
            'groupe' => $object->getGroupe()?->getId(),
            'ordre' => $object->getOrdre(),
            'has_note' => $object->hasNote(),
            'has_commentaire' => $object->hasCommentaire(),
        ];
        return $data;
    }

    public function hydrate(array $data, $object): object
    {
        $libelle = (isset($data['libelle']) and trim($data['libelle']) !== '') ? trim($data['libelle']) : null;
        $description = (isset($data['description']) and trim($data['description']) !== '') ? trim($data['description']) : null;
        $enquete = (isset($data['enquete'])) ? $this->getEnqueteService()->getEnquete($data['enquete']) : null;
        $groupe = (isset($data['groupe']) and $data['groupe'] !== "") ? $this->getGroupeService()->getGroupe($data['groupe']) : null;
        $ordre = (isset($data['ordre'])) ? trim($data['ordre']) : null;
        $hasNote = (isset($data['has_note']) and $data['has_note'] === "1");
        $hasCommentaire = (isset($data['has_commentaire']) and $data['has_commentaire'] === "1");

        /** @var Question $object */
        $object->setLibelle($libelle);
        $object->setDescription($description);
        $object->setEnquete($enquete);
        $object->setGroupe($groupe);
        $object->setOrdre($ordre);
        $object->setHasNote($hasNote);
        $object->setHasCommentaire($hasCommentaire);

        return $object;
    }

}