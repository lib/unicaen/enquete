<?php

namespace UnicaenEnquete\Form\Question;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Groupe\GroupeService;

class QuestionHydratorFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): QuestionHydrator
    {
        /**
         * @var EnqueteService $enqueteService
         * @var GroupeService $groupeService
         */
        $enqueteService = $container->get(EnqueteService::class);
        $groupeService = $container->get(GroupeService::class);

        $hydrator = new QuestionHydrator();
        $hydrator->setEnqueteService($enqueteService);
        $hydrator->setGroupeService($groupeService);
        return $hydrator;
    }
}