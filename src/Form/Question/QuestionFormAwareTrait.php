<?php

namespace UnicaenEnquete\Form\Question;

trait QuestionFormAwareTrait
{

    private QuestionForm $questionForm;

    public function getQuestionForm(): QuestionForm
    {
        return $this->questionForm;
    }

    public function setQuestionForm(QuestionForm $enqueteQuestionForm): void
    {
        $this->questionForm = $enqueteQuestionForm;
    }

}