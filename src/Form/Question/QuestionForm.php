<?php

namespace UnicaenEnquete\Form\Question;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenEnquete\Entity\Db\Enquete;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Groupe\GroupeServiceAwareTrait;
use UnicaenEnquete\Service\Question\QuestionServiceAwareTrait;

class QuestionForm extends Form
{
    use EnqueteServiceAwareTrait;
    use GroupeServiceAwareTrait;
    use QuestionServiceAwareTrait;


    public function init(): void
    {
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé de la question :",
            ],
            'attributes' => [
                'id' => 'libelle',
                'class' => 'required',
            ],
        ]);
        //description
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Complément d'information à propos de la question :",
            ],
            'attributes' => [
                'id' => 'description',
                'class' => 'tinymce',
            ],
        ]);
        //evaluation
        $this->add([
            'type' => Radio::class,
            'name' => 'has_note',
            'options' => [
                'label' => "Cette question est évaluée <span class='icon icon-obligatoire' title='Champ obligatoire'></span> <span class='icon icon-information text-info' title='Une réponse sélectionnable du type \"Satisfait\" doit être donnée'></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'value_options' => [
                    true => "Oui",
                    false => "Non",
                ],
            ],
            'attributes' => [
                'id' => 'has_note',
                'class' => 'required',
            ],
        ]);
        //commentaire
        $this->add([
            'type' => Radio::class,
            'name' => 'has_commentaire',
            'options' => [
                'label' => "Cette question peut être commentée <span class='icon icon-obligatoire'></span> <span class='icon icon-information text-info' title='Un commentaire explicatif peut être saisi'></span>:",
                'label_options' => ['disable_html_escape' => true,],
                'value_options' => [
                    true => "Oui",
                    false => "Non",
                ],
            ],
            'attributes' => [
                'id' => 'has_commentaire',
                'class' => 'required',
            ],
        ]);
        //enquete
        $this->add([
            'type' => Select::class,
            'name' => 'enquete',
            'options' => [
                'label' => "Enquête <span class='icon icon-obligatoire' title='Champ obligatoire et unique'></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'empty_option' => 'Choisissez une enquête',
                'value_options' => $this->getEnqueteService()->getEnquetesAsOptions(),
            ],
            'attributes' => [
                'id' => 'enquete',
                'class' => 'required',
            ],
        ]);
        //groupe
        $this->add([
            'type' => Select::class,
            'name' => 'groupe',
            'options' => [
                'label' => "Groupe associé à la question :",
                'empty_option' => "Aucun groupe",
                'value_options' => $this->getGroupeService()->getGroupesAsOptions(null),
            ],
            'attributes' => [
                'id' => 'groupe',
                'class' => 'show-tick',
                'data-live-search' => 'true',
            ],
        ]);
        //ordre
        $this->add([
            'type' => Text::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre de la question *:",
            ],
            'attributes' => [
                'id' => 'ordre',
                'class' => 'required',
            ],
        ]);
        //submit
        $this->add([
            'type' => Button::class,
            'name' => 'bouton',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',

            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'libelle' => ['required' => true,],
            'enquete' => ['required' => true,],
            'groupe' => ['required' => false,],
            'description' => ['required' => false,],
            'ordre' => ['required' => true,],
            'has_note' => ['required' => true,],
            'has_commentaire' => ['required' => true,],
        ]));
    }

    public function setOldCode($value): void
    {
        $this->get('old-code')->setValue($value);
    }

    public function reinitGroupesWithEnquete(Enquete $enquete): void
    {
        $options = $this->getGroupeService()->getGroupesAsOptions($enquete);
        /** @var Select $select */
        $select = $this->get('groupe');
        $select->setValueOptions($options);
    }
}