<?php

namespace UnicaenEnquete\Form\Question;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Groupe\GroupeService;
use UnicaenEnquete\Service\Question\QuestionService;

class QuestionFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): QuestionForm
    {
        /**
         * @var EnqueteService $enqueteService
         * @var GroupeService $groupeService
         * @var QuestionService $questionService
         */
        $enqueteService = $container->get(EnqueteService::class);
        $groupeService = $container->get(GroupeService::class);
        $questionService = $container->get(QuestionService::class);

        /** @var QuestionHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(QuestionHydrator::class);

        $form = new QuestionForm();
        $form->setEnqueteService($enqueteService);
        $form->setGroupeService($groupeService);
        $form->setQuestionService($questionService);
        $form->setHydrator($hydrator);
        return $form;
    }
}