<?php

namespace UnicaenEnquete\Form\Groupe;

use Laminas\Hydrator\HydratorInterface;
use UnicaenEnquete\Entity\Db\Groupe;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;

class GroupeHydrator implements HydratorInterface
{
    use EnqueteServiceAwareTrait;

    public function extract(object $object): array
    {
        /** @var Groupe $object */
        $data = [
            'enquete' => $object->getEnquete()?->getId(),
            'libelle' => $object->getLibelle(),
            'code' => $object->getCode(),
            'description' => $object->getDescription(),
            'ordre' => $object->getOrdre(),
        ];
        return $data;
    }

    public function hydrate(array $data, $object): object
    {
        $enquete = (isset($data['enquete'])) ? $this->getEnqueteService()->getEnquete($data['enquete']) : null;
        $libelle = (isset($data['libelle']) and trim($data['libelle']) !== '') ? trim($data['libelle']) : null;
        $code = (isset($data['code']) and trim($data['code']) !== '') ? trim($data['code']) : null;
        $description = (isset($data['description']) and trim($data['description']) !== '') ? trim($data['description']) : null;
        $ordre = (isset($data['ordre'])) ? trim($data['ordre']) : null;

        /** @var Groupe $object */
        $object->setEnquete($enquete);
        $object->setLibelle($libelle);
        $object->setCode($code);
        $object->setDescription($description);
        $object->setOrdre($ordre);

        return $object;
    }

}