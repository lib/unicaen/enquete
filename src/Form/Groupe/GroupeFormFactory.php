<?php

namespace UnicaenEnquete\Form\Groupe;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Groupe\GroupeService;

class GroupeFormFactory
{

    /**
     * @param ContainerInterface $container
     * @return GroupeForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): GroupeForm
    {
        /**
         * @var EnqueteService $enqueteService
         * @var GroupeService $groupeService
         * @var GroupeHydrator $hydrator
         */
        $enqueteService = $container->get(EnqueteService::class);
        $groupeService = $container->get(GroupeService::class);
        $hydrator = $container->get('HydratorManager')->get(GroupeHydrator::class);

        $form = new GroupeForm();
        $form->setEnqueteService($enqueteService);
        $form->setGroupeService($groupeService);
        $form->setHydrator($hydrator);
        return $form;
    }
}