<?php

namespace UnicaenEnquete\Form\Groupe;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Callback;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;
use UnicaenEnquete\Service\Groupe\GroupeServiceAwareTrait;

class GroupeForm extends Form
{
    use EnqueteServiceAwareTrait;
    use GroupeServiceAwareTrait;

    public function init(): void
    {
        //enquete
        $this->add([
            'type' => Select::class,
            'name' => 'enquete',
            'options' => [
                'label' => "Enquête <span class='icon icon-obligatoire' title='Champ obligatoire et unique'></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'empty_option' => 'Choisissez une enquête',
                'value_options' => $this->getEnqueteService()->getEnquetesAsOptions(),
            ],
            'attributes' => [
                'id' => 'enquete',
                'class' => 'required',
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'libelle',
            'options' => [
                'label' => "Libellé du groupe <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'libelle',
                'class' => 'required',
            ],
        ]);
        //code
        $this->add([
            'type' => Text::class,
            'name' => 'code',
            'options' => [
                'label' => "Code du groupe  <span class='icon icon-obligatoire' title='champ obligatoire et unique'></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'code',
            ],
        ]);
        $this->add([
            'name' => 'old-code',
            'type' => Hidden::class,
            'attributes' => [
                'value' => "",
            ],
        ]);
        //description
        $this->add([
            'type' => Textarea::class,
            'name' => 'description',
            'options' => [
                'label' => "Complément d'information à propos du groupe :",
            ],
            'attributes' => [
                'id' => 'description',
                'class' => 'tinymce',
            ],
        ]);
        //ordre
        $this->add([
            'type' => Text::class,
            'name' => 'ordre',
            'options' => [
                'label' => "Ordre du groupe <span class='icon icon-obligatoire' title='Champ obligatoire'></span> :",
                'label_options' => ['disable_html_escape' => true,],
            ],
            'attributes' => [
                'id' => 'ordre',
                'class' => 'required',
            ],
        ]);
        //submit
        $this->add([
            'type' => Button::class,
            'name' => 'bouton',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',

            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'enquete' => ['required' => true,],
            'libelle' => ['required' => true,],
            'code' => [
                'required' => true,
                'validators' => [[
                    'name' => Callback::class,
                    'options' => [
                        'messages' => [
                            Callback::INVALID_VALUE => "Ce code existe déjà",
                        ],
                        'callback' => function ($value, $context = []) {
                            if ($value == $context['old-code']) return true;
                            return ($this->getEnqueteService()->getEnqueteByCode($value) === null);
                        },
                        //'break_chain_on_failure' => true,
                    ],
                ]],
            ],
            'old-code' => ['required' => false,],
            'description' => ['required' => false,],
            'ordre' => ['required' => true,],
        ]));
    }

    public function setOldCode($value): void
    {
        $this->get('old-code')->setValue($value);
    }
}