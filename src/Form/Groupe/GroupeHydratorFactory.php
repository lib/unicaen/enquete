<?php

namespace UnicaenEnquete\Form\Groupe;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;

class GroupeHydratorFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): GroupeHydrator
    {
        /** @var EnqueteService $enqueteService */
        $enqueteService = $container->get(EnqueteService::class);

        $hydrator = new GroupeHydrator();
        $hydrator->setEnqueteService($enqueteService);
        return $hydrator;
    }
}