<?php

namespace UnicaenEnquete\Form\Instance;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;

class InstanceHydratorFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InstanceHydrator
    {
        /** @var EnqueteService $enqueteService */
        $enqueteService = $container->get(EnqueteService::class);

        $hydrator = new InstanceHydrator();
        $hydrator->setEnqueteService($enqueteService);
        return $hydrator;
    }
}