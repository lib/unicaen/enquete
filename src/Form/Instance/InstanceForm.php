<?php

namespace UnicaenEnquete\Form\Instance;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;

class InstanceForm extends Form
{

    use EnqueteServiceAwareTrait;

    public function init(): void
    {
        //enquete
        $this->add([
            'type' => Select::class,
            'name' => 'enquete',
            'options' => [
                'label' => "Enquête <span class='icon icon-obligatoire' title='Champ obligatoire et unique'></span> :",
                'label_options' => ['disable_html_escape' => true,],
                'empty_option' => 'Choisissez une enquête',
                'value_options' => $this->getEnqueteService()->getEnquetesAsOptions(),
            ],
            'attributes' => [
                'id' => 'enquete',
                'class' => 'required',
            ],
        ]);
        //submit
        $this->add([
            'type' => Button::class,
            'name' => 'bouton',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'enquete' => ['required' => true,],
        ]));
    }
}