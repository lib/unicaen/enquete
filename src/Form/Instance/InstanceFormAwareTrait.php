<?php

namespace UnicaenEnquete\Form\Instance;

trait InstanceFormAwareTrait
{

    private InstanceForm $instanceForm;

    public function getInstanceForm(): InstanceForm
    {
        return $this->instanceForm;
    }

    public function setInstanceForm(InstanceForm $instanceForm): void
    {
        $this->instanceForm = $instanceForm;
    }

}