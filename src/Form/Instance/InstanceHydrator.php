<?php

namespace UnicaenEnquete\Form\Instance;

use Laminas\Hydrator\HydratorInterface;
use UnicaenEnquete\Entity\Db\Instance;
use UnicaenEnquete\Service\Enquete\EnqueteServiceAwareTrait;

class InstanceHydrator implements HydratorInterface
{
    use EnqueteServiceAwareTrait;

    public function extract(object $object): array
    {
        /** @var Instance $object */
        $data = [
            'enquete' => $object->getEnquete()?->getId(),
        ];
        return $data;
    }

    public function hydrate(array $data, $object): object
    {
        $enquete = (isset($data['enquete'])) ? $this->getEnqueteService()->getEnquete($data['enquete']) : null;

        /** @var Instance $object */
        $object->setEnquete($enquete);

        return $object;
    }

}