<?php

namespace UnicaenEnquete\Form\Instance;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Enquete\EnqueteService;

class InstanceFormFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InstanceForm
    {
        /**
         * @var EnqueteService $enqueteService
         * @var InstanceHydrator $hydrator
         */
        $enqueteService = $container->get(EnqueteService::class);
        $hydrator = $container->get('HydratorManager')->get(InstanceHydrator::class);

        $form = new InstanceForm();
        $form->setEnqueteService($enqueteService);
        $form->setHydrator($hydrator);
        return $form;
    }
}