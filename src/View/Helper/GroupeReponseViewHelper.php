<?php

namespace UnicaenEnquete\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEnquete\Entity\Db\Groupe;

class GroupeReponseViewHelper extends AbstractHelper
{

    public function __invoke(Groupe $groupe, array $reponses, array $options = []): Partial|string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('groupe-reponse', ['groupe' => $groupe, 'reponses' => $reponses, 'options' => $options]);
    }
}