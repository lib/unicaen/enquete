<?php

namespace UnicaenEnquete\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEnquete\Entity\Db\Instance;
use UnicaenEnquete\Service\Reponse\ReponseServiceAwareTrait;

class InstanceReponseViewHelper extends AbstractHelper
{
    use ReponseServiceAwareTrait;

    public function __invoke(Instance $instance, array $options = []): Partial|string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        $reponses = $this->getReponseService()->getReponsesByInstance($instance);

        return $view->partial('instance-reponse', ['instance' => $instance, 'reponses' => $reponses, 'options' => $options]);
    }
}