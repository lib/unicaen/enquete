<?php

namespace UnicaenEnquete\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEnquete\Entity\Db\Question;
use UnicaenEnquete\Entity\Db\Reponse;

class QuestionReponseViewHelper extends AbstractHelper
{

    public function __invoke(Question $question, ?Reponse $reponse = null, array $options = []): Partial|string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('question-reponse', ['question' => $question, 'reponse' => $reponse, 'options' => $options]);
    }

}