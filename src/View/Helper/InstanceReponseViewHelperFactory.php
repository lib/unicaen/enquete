<?php

namespace UnicaenEnquete\View\Helper;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEnquete\Service\Reponse\ReponseService;

class InstanceReponseViewHelperFactory
{

    /**
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container): InstanceReponseViewHelper
    {
        /**
         * @var ReponseService $reponseService
         */
        $reponseService = $container->get(ReponseService::class);

        $helper = new InstanceReponseViewHelper();
        $helper->setReponseService($reponseService);
        return $helper;
    }
}