-- TTTTTTTTTTTTTTTTTTTTTTT         AAA               BBBBBBBBBBBBBBBBB   LLLLLLLLLLL             EEEEEEEEEEEEEEEEEEEEEE
-- T:::::::::::::::::::::T        A:::A              B::::::::::::::::B  L:::::::::L             E::::::::::::::::::::E
-- T:::::::::::::::::::::T       A:::::A             B::::::BBBBBB:::::B L:::::::::L             E::::::::::::::::::::E
-- T:::::TT:::::::TT:::::T      A:::::::A            BB:::::B     B:::::BLL:::::::LL             EE::::::EEEEEEEEE::::E
-- TTTTTT  T:::::T  TTTTTT     A:::::::::A             B::::B     B:::::B  L:::::L                 E:::::E       EEEEEE
--         T:::::T            A:::::A:::::A            B::::B     B:::::B  L:::::L                 E:::::E
--         T:::::T           A:::::A A:::::A           B::::BBBBBB:::::B   L:::::L                 E::::::EEEEEEEEEE
--         T:::::T          A:::::A   A:::::A          B:::::::::::::BB    L:::::L                 E:::::::::::::::E
--         T:::::T         A:::::A     A:::::A         B::::BBBBBB:::::B   L:::::L                 E:::::::::::::::E
--         T:::::T        A:::::AAAAAAAAA:::::A        B::::B     B:::::B  L:::::L                 E::::::EEEEEEEEEE
--         T:::::T       A:::::::::::::::::::::A       B::::B     B:::::B  L:::::L                 E:::::E
--         T:::::T      A:::::AAAAAAAAAAAAA:::::A      B::::B     B:::::B  L:::::L         LLLLLL  E:::::E       EEEEEE
--       TT:::::::TT   A:::::A             A:::::A   BB:::::BBBBBB::::::BLL:::::::LLLLLLLLL:::::LEE::::::EEEEEEEE:::::E
--       T:::::::::T  A:::::A               A:::::A  B:::::::::::::::::B L::::::::::::::::::::::LE::::::::::::::::::::E
--       T:::::::::T A:::::A                 A:::::A B::::::::::::::::B  L::::::::::::::::::::::LE::::::::::::::::::::E
--       TTTTTTTTTTTAAAAAAA                   AAAAAAABBBBBBBBBBBBBBBBB   LLLLLLLLLLLLLLLLLLLLLLLLEEEEEEEEEEEEEEEEEEEEEE

create table unicaen_enquete_enquete
(
    id                    serial
        constraint unicaen_enquete_enquete_pk
            primary key,
    titre                 varchar(1024)           not null,
    description           text,
    histo_creation        timestamp default now() not null,
    histo_createur_id     integer   default 0     not null
        constraint unicaen_enquete_enquete_unicaen_utilisateur_user_id_fk
            references unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_modificateur_id integer
        constraint unicaen_enquete_enquete_unicaen_utilisateur_user_id_fk_2
            references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint unicaen_enquete_enquete_unicaen_utilisateur_user_id_fk_3
            references unicaen_utilisateur_user,
    code                  varchar(1024)           not null
);

create table unicaen_enquete_groupe
(
    id                    serial
        constraint formation_enquete_categorie_pkey
            primary key,
    libelle               varchar(1024)           not null,
    description           text,
    ordre                 integer                 not null,
    histo_createur_id     integer   default 0     not null
        constraint formation_enquete_categorie_utilisateur_id_fk_1
            references unicaen_utilisateur_user,
    histo_creation        timestamp default now() not null,
    histo_modificateur_id integer
        constraint formation_enquete_categorie_utilisateur_id_fk_2
            references unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_destructeur_id  integer
        constraint formation_enquete_categorie_utilisateur_id_fk_3
            references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    enquete_id            integer   default 1     not null
        constraint unicaen_enquete_groupe_unicaen_enquete_enquete_id_fk
            references unicaen_enquete_enquete
            on delete cascade,
    code                  varchar(256)            not null
);
create unique index formation_enquete_categorie_id_uindex on unicaen_enquete_groupe (id);
create unique index unicaen_enquete_groupe_code_uindex on unicaen_enquete_groupe (code);
create index unicaen_enquete_groupe_enquete_id_index on unicaen_enquete_groupe (enquete_id);

create table unicaen_enquete_question
(
    id                    serial
        constraint formation_enquete_question_pkey
            primary key,
    groupe_id             integer
        constraint formation_enquete_question_formation_enquete_categorie_id_fk
            references unicaen_enquete_groupe
            on delete set null,
    enquete_id            integer   default 1     not null
        constraint unicaen_enquete_question_unicaen_enquete_enquete_id_fk
            references unicaen_enquete_enquete
            on delete cascade,
    libelle               varchar(1024)           not null,
    description           text,
    ordre                 integer                 not null,
    has_note              boolean   default true  not null,
    has_commentaire       boolean   default true  not null,
    histo_createur_id     integer   default 0     not null
        constraint formation_enquete_question_utilisateur_id_fk_1
            references unicaen_utilisateur_user,
    histo_creation        timestamp default now() not null,
    histo_modificateur_id integer
        constraint formation_enquete_question_utilisateur_id_fk_2
            references unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_destructeur_id  integer
        constraint formation_enquete_question_utilisateur_id_fk_3
            references unicaen_utilisateur_user,
    histo_destruction     timestamp

);
create unique index formation_enquete_question_id_uindex on unicaen_enquete_question (id);
create index unicaen_enquete_question_enquete_id_index on unicaen_enquete_question (enquete_id);
create index unicaen_enquete_question_groupe_id_index on unicaen_enquete_question (groupe_id);
create index unicaen_enquete_enquete_code_index on unicaen_enquete_enquete (code);

create table unicaen_enquete_instance
(
    id                    serial
        constraint unicaen_enquete_instance_pk
            primary key,
    enquete_id            integer                 not null
        constraint unicaen_enquete_instance_unicaen_enquete_enquete_id_fk
            references unicaen_enquete_enquete
            on delete cascade,
    validation            timestamp,
    histo_creation        timestamp default now() not null,
    histo_createur_id     integer   default 0     not null
        constraint unicaen_enquete_instance_unicaen_utilisateur_user_id_fk
            references unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_modificateur_id integer
        constraint unicaen_enquete_instance_unicaen_utilisateur_user_id_fk_2
            references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint unicaen_enquete_instance_unicaen_utilisateur_user_id_fk_3
            references unicaen_utilisateur_user
);
create index unicaen_enquete_instance_enquete_id_index on unicaen_enquete_instance (enquete_id);

create table unicaen_enquete_reponse
(
    id                    serial
        constraint unicaen_enquete_reponse_pk
            primary key,
    instance_id           integer                 not null
        constraint unicaen_enquete_reponse_unicaen_enquete_instance_id_fk
            references unicaen_enquete_instance
            on delete cascade,
    question_id           integer                 not null
        constraint unicaen_enquete_reponse_unicaen_enquete_question_id_fk
            references unicaen_enquete_question
            on delete cascade,
    reponse               varchar(1024),
    commentaire           text,
    histo_creation        timestamp default now() not null,
    histo_createur_id     integer   default 0     not null
        constraint unicaen_enquete_reponse_unicaen_utilisateur_user_id_fk
            references unicaen_utilisateur_user,
    histo_modification    timestamp,
    histo_modificateur_id integer
        constraint unicaen_enquete_reponse_unicaen_utilisateur_user_id_fk_2
            references unicaen_utilisateur_user,
    histo_destruction     timestamp,
    histo_destructeur_id  integer
        constraint unicaen_enquete_reponse_unicaen_utilisateur_user_id_fk_3
            references unicaen_utilisateur_user
);
create index unicaen_enquete_reponse_instance_id_index on unicaen_enquete_reponse (instance_id);
create index unicaen_enquete_reponse_question_id_index on unicaen_enquete_reponse (question_id);


-- IIIIIIIIIINNNNNNNN        NNNNNNNN   SSSSSSSSSSSSSSS EEEEEEEEEEEEEEEEEEEEEERRRRRRRRRRRRRRRRR   TTTTTTTTTTTTTTTTTTTTTTT
-- I::::::::IN:::::::N       N::::::N SS:::::::::::::::SE::::::::::::::::::::ER::::::::::::::::R  T:::::::::::::::::::::T
-- I::::::::IN::::::::N      N::::::NS:::::SSSSSS::::::SE::::::::::::::::::::ER::::::RRRRRR:::::R T:::::::::::::::::::::T
-- II::::::IIN:::::::::N     N::::::NS:::::S     SSSSSSSEE::::::EEEEEEEEE::::ERR:::::R     R:::::RT:::::TT:::::::TT:::::T
--   I::::I  N::::::::::N    N::::::NS:::::S              E:::::E       EEEEEE  R::::R     R:::::RTTTTTT  T:::::T  TTTTTT
--   I::::I  N:::::::::::N   N::::::NS:::::S              E:::::E               R::::R     R:::::R        T:::::T
--   I::::I  N:::::::N::::N  N::::::N S::::SSSS           E::::::EEEEEEEEEE     R::::RRRRRR:::::R         T:::::T
--   I::::I  N::::::N N::::N N::::::N  SS::::::SSSSS      E:::::::::::::::E     R:::::::::::::RR          T:::::T
--   I::::I  N::::::N  N::::N:::::::N    SSS::::::::SS    E:::::::::::::::E     R::::RRRRRR:::::R         T:::::T
--   I::::I  N::::::N   N:::::::::::N       SSSSSS::::S   E::::::EEEEEEEEEE     R::::R     R:::::R        T:::::T
--   I::::I  N::::::N    N::::::::::N            S:::::S  E:::::E               R::::R     R:::::R        T:::::T
--   I::::I  N::::::N     N:::::::::N            S:::::S  E:::::E       EEEEEE  R::::R     R:::::R        T:::::T
-- II::::::IIN::::::N      N::::::::NSSSSSSS     S:::::SEE::::::EEEEEEEE:::::ERR:::::R     R:::::R      TT:::::::TT
-- I::::::::IN::::::N       N:::::::NS::::::SSSSSS:::::SE::::::::::::::::::::ER::::::R     R:::::R      T:::::::::T
-- I::::::::IN::::::N        N::::::NS:::::::::::::::SS E::::::::::::::::::::ER::::::R     R:::::R      T:::::::::T
-- IIIIIIIIIINNNNNNNN         NNNNNNN SSSSSSSSSSSSSSS   EEEEEEEEEEEEEEEEEEEEEERRRRRRRR     RRRRRRR      TTTTTTTTTTT


INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
    ('enquete', 'Enquête - Gestion des enquêtes', 'UnicaenEnquete\Provider\Privilege', 100);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'enquete_index', 'Accéder à l''index', 10 UNION
    SELECT 'enquete_afficher', 'Afficher', 20 UNION
    SELECT 'enquete_ajouter', 'Ajouter', 30 UNION
    SELECT 'enquete_modifier', 'Modifier', 40 UNION
    SELECT 'enquete_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'enquete_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'enquete';

INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
    ('egroupe', 'Enquête - Gestions des groupes de questions', 'UnicaenEnquete\Provider\Privilege', 200);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'groupe_index', 'Accéder à l''index', 10 UNION
    SELECT 'groupe_afficher', 'Afficher', 20 UNION
    SELECT 'groupe_ajouter', 'Ajouter', 30 UNION
    SELECT 'groupe_modifier', 'Modifier', 40 UNION
    SELECT 'groupe_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'groupe_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'egroupe';

INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
    ('question', 'Enquête - Gestions des questions', 'UnicaenEnquete\Provider\Privilege', 300);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'question_index', 'Accéder à l''index', 10 UNION
    SELECT 'question_afficher', 'Afficher', 20 UNION
    SELECT 'question_ajouter', 'Ajouter', 30 UNION
    SELECT 'question_modifier', 'Modifier', 40 UNION
    SELECT 'question_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'question_supprimer', 'Supprimer', 60
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'question';

INSERT INTO unicaen_privilege_categorie (code, libelle, namespace, ordre) VALUES
    ('einstance', 'Enquête - Gestion des instances', 'UnicaenEnquete\Provider\Privilege', 1100);
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'instance_index', 'Accéder à l''index', 10 UNION
    SELECT 'instance_afficher', 'Afficher', 20 UNION
    SELECT 'instance_ajouter', 'Ajouter', 30 UNION
    SELECT 'instance_modifier', 'Modifier', 40 UNION
    SELECT 'instance_historiser', 'Historiser/Restaurer', 50 UNION
    SELECT 'instance_supprimer', 'Supprimer', 60

)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'einstance';