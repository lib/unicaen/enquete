<?php

namespace UnicaenEnquete;

use UnicaenEnquete\Controller\InstanceController;
use UnicaenEnquete\Controller\InstanceControllerFactory;
use UnicaenEnquete\Form\Instance\InstanceForm;
use UnicaenEnquete\Form\Instance\InstanceFormFactory;
use UnicaenEnquete\Form\Instance\InstanceHydrator;
use UnicaenEnquete\Form\Instance\InstanceHydratorFactory;
use UnicaenEnquete\Provider\Privilege\EinstancePrivileges;
use UnicaenEnquete\Service\Instance\InstanceService;
use UnicaenEnquete\Service\Instance\InstanceServiceFactory;
use UnicaenEnquete\View\Helper\InstanceReponseViewHelperFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => InstanceController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EinstancePrivileges::INSTANCE_INDEX,
                    ],
                ],
                [
                    'controller' => InstanceController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        EinstancePrivileges::INSTANCE_AFFICHER,
                    ],
                ],
                [
                    'controller' => InstanceController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        EinstancePrivileges::INSTANCE_AJOUTER,
                    ],
                ],
                [
                    'controller' => InstanceController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        EinstancePrivileges::INSTANCE_MODIFIER,
                    ],
                ],
                [
                    'controller' => InstanceController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        EinstancePrivileges::INSTANCE_HISTORISER,
                    ],
                ],
                [
                    'controller' => InstanceController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        EinstancePrivileges::INSTANCE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'enquete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/enquete',
                    'defaults' => [
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'instance' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/instance',
                            'defaults' => [
                                /** @see InstanceController::indexAction() */
                                'controller' => InstanceController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:instance',
                                    'defaults' => [
                                        /** @see InstanceController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter[/:enquete]',
                                    'defaults' => [
                                        /** @see InstanceController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:instance',
                                    'defaults' => [
                                        /** @see InstanceController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:instance',
                                    'defaults' => [
                                        /** @see InstanceController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:instance',
                                    'defaults' => [
                                        /** @see InstanceController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:instance',
                                    'defaults' => [
                                        /** @see InstanceController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [

            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            InstanceService::class => InstanceServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            InstanceController::class => InstanceControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            InstanceForm::class => InstanceFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            InstanceHydrator::class => InstanceHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
        'factories' => [
            'instanceReponse' => InstanceReponseViewHelperFactory::class,
        ],
    ],

];