<?php

namespace UnicaenEnquete;

use UnicaenEnquete\Controller\GroupeController;
use UnicaenEnquete\Controller\GroupeControllerFactory;
use UnicaenEnquete\Form\Groupe\GroupeForm;
use UnicaenEnquete\Form\Groupe\GroupeFormFactory;
use UnicaenEnquete\Form\Groupe\GroupeHydrator;
use UnicaenEnquete\Form\Groupe\GroupeHydratorFactory;
use UnicaenEnquete\Provider\Privilege\EgroupePrivileges;
use UnicaenEnquete\Service\Groupe\GroupeService;
use UnicaenEnquete\Service\Groupe\GroupeServiceFactory;
use UnicaenEnquete\View\Helper\GroupeEditionViewHelper;
use UnicaenEnquete\View\Helper\GroupeReponseViewHelper;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EgroupePrivileges::GROUPE_INDEX,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        EgroupePrivileges::GROUPE_AFFICHER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        EgroupePrivileges::GROUPE_AJOUTER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        EgroupePrivileges::GROUPE_MODIFIER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        EgroupePrivileges::GROUPE_HISTORISER,
                    ],
                ],
                [
                    'controller' => GroupeController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        EgroupePrivileges::GROUPE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'enquete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/enquete',
                    'defaults' => [
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'groupe' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/groupe',
                            'defaults' => [
                                /** @see GroupeController::indexAction() */
                                'controller' => GroupeController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter[/:enquete]',
                                    'defaults' => [
                                        /** @see GroupeController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:groupe',
                                    'defaults' => [
                                        /** @see GroupeController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [

            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            GroupeService::class => GroupeServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            GroupeController::class => GroupeControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            GroupeForm::class => GroupeFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            GroupeHydrator::class => GroupeHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            'groupeEdition' => GroupeEditionViewHelper::class,
            'groupeReponse' => GroupeReponseViewHelper::class,
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],

];