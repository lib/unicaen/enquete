<?php

namespace UnicaenEnquete;

use UnicaenEnquete\Controller\ResultatController;
use UnicaenEnquete\Controller\ResultatControllerFactory;
use UnicaenEnquete\Provider\Privilege\EnquetePrivileges;
use UnicaenEnquete\Service\Resultat\ResultatService;
use UnicaenEnquete\Service\Resultat\ResultatServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => ResultatController::class,
                    'action' => [
                        'resultats',
                    ],
                    'privileges' => [
                        EnquetePrivileges::ENQUETE_INDEX,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'enquete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/enquete',
                    'defaults' => [
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'resultats' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/resultats/:enquete',
                            'defaults' => [
                                /** @see ResultatController::resultatsAction() */
                                'controller' => ResultatController::class,
                                'action' => 'resultats',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                        ],
                    ],
                ],
            ],
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [

            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            ResultatService::class => ResultatServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            ResultatController::class => ResultatControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ]

];