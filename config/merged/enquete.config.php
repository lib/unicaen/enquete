<?php

namespace UnicaenEnquete;

use UnicaenEnquete\Controller\EnqueteController;
use UnicaenEnquete\Controller\EnqueteControllerFactory;
use UnicaenEnquete\Form\Enquete\EnqueteForm;
use UnicaenEnquete\Form\Enquete\EnqueteFormFactory;
use UnicaenEnquete\Form\Enquete\EnqueteHydrator;
use UnicaenEnquete\Form\Enquete\EnqueteHydratorFactory;
use UnicaenEnquete\Provider\Privilege\EnquetePrivileges;
use UnicaenEnquete\Service\Enquete\EnqueteService;
use UnicaenEnquete\Service\Enquete\EnqueteServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EnqueteController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        EnquetePrivileges::ENQUETE_INDEX,
                    ],
                ],
                [
                    'controller' => EnqueteController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        EnquetePrivileges::ENQUETE_AFFICHER,
                    ],
                ],
                [
                    'controller' => EnqueteController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        EnquetePrivileges::ENQUETE_AJOUTER,
                    ],
                ],
                [
                    'controller' => EnqueteController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        EnquetePrivileges::ENQUETE_MODIFIER,
                    ],
                ],
                [
                    'controller' => EnqueteController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        EnquetePrivileges::ENQUETE_HISTORISER,
                    ],
                ],
                [
                    'controller' => EnqueteController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        EnquetePrivileges::ENQUETE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'enquete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/enquete',
                    'defaults' => [
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'enquete' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/enquete',
                            'defaults' => [
                                /** @see EnqueteController::indexAction() */
                                'controller' => EnqueteController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:enquete',
                                    'defaults' => [
                                        /** @see EnqueteController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/ajouter',
                                    'defaults' => [
                                        /** @see EnqueteController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:enquete',
                                    'defaults' => [
                                        /** @see EnqueteController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:enquete',
                                    'defaults' => [
                                        /** @see EnqueteController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:enquete',
                                    'defaults' => [
                                        /** @see EnqueteController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:enquete',
                                    'defaults' => [
                                        /** @see EnqueteController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [

            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            EnqueteService::class => EnqueteServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            EnqueteController::class => EnqueteControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            EnqueteForm::class => EnqueteFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            EnqueteHydrator::class => EnqueteHydratorFactory::class,
        ],
    ]

];