<?php

namespace UnicaenEnquete;

use UnicaenEnquete\Service\Reponse\ReponseService;
use UnicaenEnquete\Service\Reponse\ReponseServiceFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'enquete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/enquete',
                    'defaults' => [
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                ],
            ],
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [

            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            ReponseService::class => ReponseServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
        ],
    ],
    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'factories' => [
        ],
    ]

];