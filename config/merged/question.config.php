<?php

namespace UnicaenEnquete;

use UnicaenEnquete\Controller\QuestionController;
use UnicaenEnquete\Controller\QuestionControllerFactory;
use UnicaenEnquete\Form\Question\QuestionForm;
use UnicaenEnquete\Form\Question\QuestionFormFactory;
use UnicaenEnquete\Form\Question\QuestionHydrator;
use UnicaenEnquete\Form\Question\QuestionHydratorFactory;
use UnicaenEnquete\Provider\Privilege\QuestionPrivileges;
use UnicaenEnquete\Service\Question\QuestionService;
use UnicaenEnquete\Service\Question\QuestionServiceFactory;
use UnicaenEnquete\View\Helper\QuestionEditionViewHelper;
use UnicaenEnquete\View\Helper\QuestionReponseViewHelper;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        QuestionPrivileges::QUESTION_INDEX,
                    ],
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        QuestionPrivileges::QUESTION_AFFICHER,
                    ],
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        QuestionPrivileges::QUESTION_AJOUTER,
                    ],
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        QuestionPrivileges::QUESTION_MODIFIER,
                    ],
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'historiser',
                        'restaurer',
                    ],
                    'privileges' => [
                        QuestionPrivileges::QUESTION_HISTORISER,
                    ],
                ],
                [
                    'controller' => QuestionController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        QuestionPrivileges::QUESTION_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'enquete' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/enquete',
                    'defaults' => [
                    ],
                ],
                'may_terminate' => false,
                'child_routes' => [
                    'question' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/question',
                            'defaults' => [
                                /** @see QuestionController::indexAction() */
                                'controller' => QuestionController::class,
                                'action' => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/afficher/:question',
                                    'defaults' => [
                                        /** @see QuestionController::afficherAction() */
                                        'action' => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/ajouter[/:enquete[/:groupe]]',
                                    'defaults' => [
                                        /** @see QuestionController::ajouterAction() */
                                        'action' => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/modifier/:question',
                                    'defaults' => [
                                        /** @see QuestionController::modifierAction() */
                                        'action' => 'modifier',
                                    ],
                                ],
                            ],
                            'historiser' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/historiser/:question',
                                    'defaults' => [
                                        /** @see QuestionController::historiserAction() */
                                        'action' => 'historiser',
                                    ],
                                ],
                            ],
                            'restaurer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/restaurer/:question',
                                    'defaults' => [
                                        /** @see QuestionController::restaurerAction() */
                                        'action' => 'restaurer',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/supprimer/:question',
                                    'defaults' => [
                                        /** @see QuestionController::supprimerAction() */
                                        'action' => 'supprimer',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [

            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            QuestionService::class => QuestionServiceFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            QuestionController::class => QuestionControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            QuestionForm::class => QuestionFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            QuestionHydrator::class => QuestionHydratorFactory::class,
        ],
    ],

    'view_helpers' => [
        'invokables' => [
            'questionEdition' => QuestionEditionViewHelper::class,
            'questionReponse' => QuestionReponseViewHelper::class,
        ],
        'factories' => [
        ],
        'aliases' => [
        ],
    ],
];