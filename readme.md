Module Unicaen Enquete
=======================

# Description

La bibliothèque `unicaen/enquete` est en charge de la modélisation d'enquète : partie questionnaire, partie reponse et partie statistique.
Attention pour la partie statistique la biliothèque javascript `Chart.js` est utilisée (testée jusqu'à la version 2.9.4).

## Partie questionnaire

Les questionnaires sont modélisés avec trois entités :
1. les enquêtes `Enquete` 
2. les groupes `Groupe`
3. les questions `Question`

![tables associées à la partie questionnaire](img/Enquete_questionnaire.drawio.svg)


### `Enquete` 

Les enquêtes sont les éléments centraux qui serviront à regrouper les questions, les instances de réponses et les statistiques.

```php
    private ?string $titre = null; 
    private ?string $code = null;
    private ?string $description = null;
    private Collection $groupes;
    private Collection $questions;
```

Les enquêtes possèdent un `titre` qui sera affiché, un `code` unique qui peut être utilise par les méthodes pour récupérer une enquête et une `description` éventuelle.

### `Groupe`

Il s'agit de collection de questions afin de regrouper celles-ci par thèmatique par exemple (le lieux de formation, le formateur, les supports, ...).

```php
    private ?string $libelle = null;
    private ?string $code = null;
    private ?string $description = null;
    
    private ?Enquete $enquete = null;
    private ?int $ordre = null;
    
    private Collection $questions;
```

Le groupe comme l'enquête est décrit avec les trois élements : `libelle`, `code` (unique) et `description`.
Il est muni d'un lien vers l'enquete et d'un `ordre` permettant de contrôle la position du groupe dans le questionnaire.

### `Question`

Les questions sont directement un élément représentant les questions qui seront répondus par les utilisateurs (*dah !*).
Une question est composée de deux parties (pouvant être désactivées question par question):
- une évaluation (par exemple "Satisfait", "Peu satisfait", ...)
- un commentaire 

```php
    private ?string $libelle = null;
    private ?string $description = null;

    private ?Enquete $enquete = null;
    private ?Groupe $groupe = null;
    private ?int $ordre = null;

    private bool $hasNote = true;
    private bool $hasCommentaire = true;
```

Les questions sont décrite via un libelle et un description.
Elles sont muni d'un lien vers l'enquête (obligatoire) et un l'un vers un groupe (facultatif) et un `ordre`. 
Si une question est laissé sans groupe alors elle sera possitionné au même niveau que les groupes et intercallée en fonction de son ordre.
Si une question est liée à un groupe alors elle sera affiché dans celui-ci est elle sera classée en utilisant l'ordre dans ce groupe.
Les deux booléens `hasNote` et `hasCommentaire` permettent de contrôle l'usage respectif des évaluations et des commentaires.

### Administration et paramètrage 

Les enquêtes peuvent être déclarées via l'interface d'administration.

![exemple de questionnaire](img/modelisation.png)

Le navigation associée à ce menu d'administration est déclaré dans `config/autoload/unicaen-enquete.global.php` (à déplacer lors de l'installation du module).

## Partie reponse

Les partie réponse est portée par deux entités
1. les instances de réponse `Instance`
2. les réponses au question elle-même `Reponse`

![tables associées à la partie questionnaire](img/Enquete_reponse.drawio.svg)

### `Instance`

Les instances sont les entités agglomérantes pour représenter un réponse à un questionnaire.
Elle regroupe les réponses pour chaque question d'un même questionnaire.

```php
    private ?Enquete $enquete = null;
    private Collection $reponses;
    private ?DateTime $validation = null;
```

La validation est un simple horodatage.

### `Reponse`

Une réponse represente les éléments répondus (évaluation et commentaire) pour une question et un questionnaire.

```php
    private ?Instance $instance = null;
    private ?Question $question = null;
    
    private ?string $reponse = null;
    private ?string $commentaire = null;
```

### Répondre à un question 

La biliothèque fournit une interface pour la partie réponse sous la forme d'une aide de vue (qui sera discutée dans la prochaine section).

![exemple de reponse](img/questionnaire.png)

## Partie "statistique"

La partie statistique propose de dépouiller de façon synthétique les résultats d'une enquête dans un contexte donné.

Pour chaque question on retrouve trois types de résultats :
1. le taux de participation avec comme mesure : sans réponse, sans avis et avec avis
2. le taux de satisfaction ne considérant que les réponses données et en exluant les sans avis
3. les commentaires associés

Pour chaque type de résultats on retroue les données sous trois formes :
1. les données sous forme tabulaire
2. les données sous forme graphique
3. les données sous forme d'export csv 


![exemple de réponse](img/statistique1.png)
![exemple de réponse](img/statistique2.png)

### prérequis techniques

Comme indiqué en introduction, les représentations exploite la bibliothèque `Chart.js` qu'il faut installer dans votre application.
Pour exemple, il peut être installé dans le répertoire `public/vendor` et il faut préciser dans `module.config.php` le chargement de la bibliothèque en entête.

```php
    ...
    'public_files' => [
        'head_scripts' => [
            '201_' => 'vendor/chart-2.9.3/Chart.bundle.js',
        ],
    ],
    ...
```

### Dépuoillage et interface/trait

La méthode dépouillant les résultats `ResultatService::generateResultatArray(Enquete $enquete, array $elements)` utilise deux paramètres :
1. `$enquete` qui permet de filtrer les réponses qui ne reposerait pas sur l'enquete donnée (en cas de changement d'enquête par exemple)
2. `$elements` qui est un tableau d'entité ayant une `Instance` d'attachée

Afin de faciliter le travail d'implémentation, la biliothèque fournit une interface `HasEnqueteInterface` (et le trait associé `HasEnqueteTrait`) pour des entités ayant un instance.

```php
<?php

namespace UnicaenEnquete\Entity;

use DateTime;
use UnicaenEnquete\Entity\Db\Instance;

interface HasEnqueteInterface
{
    //private ?Instance $enquete = null;
    
    public function getEnquete(): ?Instance;
    public function setEnquete(?Instance $enquete): void;

}
```

**N.B. :** Attention la méthode de dépouillage est prévu pour exploiter des instances étant validées.

# Aides de vue founies

A écrire

# Version et changelog 

**A venir à partir de la version 1.0.0 ** 

# Améliorations à venir

- Déclaration de l'aspect problèmatique d'un commentaire
- Mettre plus de javascript sur la partie modélisation de l'enquete
- Composant VueJS
- Permettre de paramètrer les choix des réponses pour la partie note
- Permettre de paramètre le texte accroché au commentaire